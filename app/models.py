from app import db
from sqlalchemy.orm import relationship


class Transaction(db.Model):
    __tablename__ = 'transactions'
    id = db.Column(db.BigInteger, primary_key=True)
    date = db.Column(db.DateTime)
    card = db.Column(db.String)
    account = db.Column(db.String)
    account_valid_to = db.Column(db.DateTime)
    client = db.Column(db.String)
    last_name = db.Column(db.String)
    first_name = db.Column(db.String)
    patronymic = db.Column(db.String)
    date_of_birth = db.Column(db.DateTime)
    passport = db.Column(db.String)
    passport_valid_to = db.Column(db.DateTime)
    phone = db.Column(db.String)
    oper_type = db.Column(db.String)
    amount = db.Column(db.Float(4))
    oper_result = db.Column(db.String)
    terminal = db.Column(db.String)
    terminal_type = db.Column(db.String)
    city = db.Column(db.String)
    address = db.Column(db.String)
    age = db.Column(db.Integer)
    count_tr_day = db.Column(db.Integer)
    count_tr_hour = db.Column(db.Integer)
    count_reject_1_hour = db.Column(db.Integer)
    time_last_tr = db.Column(db.Integer)
    changing_the_city = db.Column(db.Integer)
    frauds = db.relationship('Fraud', lazy='joined')


class Fraud(db.Model):
    __tablename__ = 'transaction_frauds'
    id = db.Column(db.BigInteger, primary_key=True)
    transaction_id = db.Column(db.Integer, db.ForeignKey('transactions.id'))
    fraud_id = db.Column(db.Integer, db.ForeignKey('frauds.id'))
    details = relationship('FraudsDetail', lazy='joined')


class FraudsDetail(db.Model):
    __tablename__ = 'frauds'
    id = db.Column(db.BigInteger, primary_key=True)
    fraud_type = db.Column(db.String)
    scores = db.Column(db.Integer)