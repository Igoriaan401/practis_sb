from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

app = Flask(__name__)
app.config[
    'SQLALCHEMY_DATABASE_URI'] = 'postgresql://gwgrjrcr:CZd5i7U3YgAiYOpoChPZTndt2GFB9_ei@arjuna.db.elephantsql.com/gwgrjrcr'
CORS(app)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

from app import models
from app import routes