from app.models import Transaction, Fraud, FraudsDetail
from app import db
import math
from sqlalchemy import func, Column, text
from datetime import datetime, date


def search_cliets(clied_id):
    result = db.session.query(Transaction).filter(Transaction.client == clied_id).all()
    data = []

    for i in result:
        data.append(format_transaction(i))

    return data


def format_transaction(transaction):
    frauds_list = transaction.frauds
    frauds = []
    for fraud in frauds_list:
        frauds_detaial = [fraud.details]
        for i in frauds_detaial:
            frauds.append({
                # "id": fraud.id,
                # "transaction_id": fraud.transaction_id,
                "fraud_type": i.fraud_type,
                "scores": i.scores
            })
    data = {
        "id": transaction.id,
        "date": transaction.date,
        "card": transaction.card,
        "account": transaction.account,
        "account_valid_to": transaction.account_valid_to,
        "client": transaction.client,
        "last_name": transaction.last_name,
        "first_name": transaction.first_name,
        "patronymic": transaction.patronymic,
        "date_of_birth": transaction.date_of_birth,
        "passport": transaction.passport,
        "passport_valid_to": transaction.passport_valid_to,
        "phone": transaction.phone,
        "oper_type": transaction.oper_type,
        "amount": transaction.amount,
        "oper_result": transaction.oper_result,
        "terminal": transaction.terminal,
        "terminal_type": transaction.terminal_type,
        "city": transaction.city,
        "address": transaction.address,
        "age": transaction.age,
        "count_tr_day": transaction.count_tr_day,
        "count_tr_hour": transaction.count_tr_hour,
        "count_reject_1_hour": transaction.count_reject_1_hour,
        "time_last_tr": transaction.time_last_tr,
        "changing_the_city": transaction.changing_the_city,
        "fraud": frauds,
    }

    return data


def paginate_transactions(url: str, order: int = 0, direction: int = 0, start: int = 1, limit: int = 10):
    if order == 1 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.id.asc()).offset(start).limit(limit).all()
    elif order == 1 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.id.desc()).offset(start).limit(limit).all()
    elif order == 2 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.date.asc()).offset(start).limit(limit).all()
    elif order == 2 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.date.desc()).offset(start).limit(limit).all()
    elif order == 3 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.account.asc()).offset(start).limit(limit).all()
    elif order == 3 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.account.desc()).offset(start).limit(limit).all()
    elif order == 4 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.client.asc()).offset(start).limit(limit).all()
    elif order == 4 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.client.desc()).offset(start).limit(limit).all()
    elif order == 5 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.passport_valid_to.asc()).offset(start).limit(limit).all()
    elif order == 5 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.passport_valid_to.desc()).offset(start).limit(limit).all()
    elif order == 6 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.phone.asc()).offset(start).limit(limit).all()
    elif order == 6 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.phone.desc()).offset(start).limit(limit).all()
    elif order == 7 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.oper_type.asc()).offset(start).limit(limit).all()
    elif order == 7 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.oper_type.desc()).offset(start).limit(limit).all()
    elif order == 8 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.amount.asc()).offset(start).limit(limit).all()
    elif order == 8 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.amount.desc()).offset(start).limit(limit).all()
    elif order == 9 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.oper_result.asc()).offset(start).limit(limit).all()
    elif order == 9 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.oper_result.desc()).offset(start).limit(limit).all()
    elif order == 10 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.city.asc()).offset(start).limit(limit).all()
    elif order == 10 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.city.desc()).offset(start).limit(limit).all()
    elif order == 11 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.count_tr_day.asc()).offset(start).limit(limit).all()
    elif order == 11 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.count_tr_day.desc()).offset(start).limit(limit).all()
    elif order == 12 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.count_tr_hour.asc()).offset(start).limit(limit).all()
    elif order == 12 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.count_tr_hour.desc()).offset(start).limit(limit).all()
    elif order == 13 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.count_reject_1_hour.asc()).offset(start).limit(limit).all()
    elif order == 13 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.count_reject_1_hour.desc()).offset(start).limit(limit).all()
    elif order == 14 and direction == 0:
        result = db.session.query(Transaction).order_by(Transaction.time_last_tr.asc()).offset(start).limit(limit).all()
    elif order == 14 and direction == 1:
        result = db.session.query(Transaction).order_by(Transaction.time_last_tr.desc()).offset(start).limit(limit).all()
    else:
        result = db.session.query(Transaction).offset(start).limit(limit).all()

    data = []

    for i in result:
        data.append(format_transaction(i))
    count = db.session.query(Transaction.id).count()

    obj = {}

    if count < start:
        obj['success'] = False
        obj['message'] = 'Error'
        return obj
    else:
        obj['success'] = True
        obj['currentPage'] = start
        obj['perPage'] = limit
        obj['totalData'] = count
        obj['totalPage'] = math.ceil(count/limit)
        #prev_link
        if start == 1:
            obj['previos'] = ''
        else:
            start_copy = max(1, start-limit)
            limit_copy = start - 1
            obj['previos'] = url + '?start=%d&limit=%d&order=%d&direction=%d' % (start_copy, limit_copy, order, direction)

        #next_link
        if start + limit > count:
            obj['next'] = ''
        else:
            start_copy = start + limit
            obj['next'] = url + '?start=%d&limit=%d&order=%d&direction=%d' % (start_copy, limit, order, direction)

        obj['transactions'] = data #[(start - 1): (start - 1 + limit)]
        return obj


def paginate_frauds(url, start, limit, types):
    if types == 1:
        result = db.session.query(Transaction) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 0.6, func.sum(FraudsDetail.scores) < 0.9).offset(start).limit(
            limit).all()

        count = db.session.query(Transaction.id) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 0.6, func.sum(FraudsDetail.scores) < 0.9).count()
    elif types == 2:
        result = db.session.query(Transaction) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 0.9, func.sum(FraudsDetail.scores) <= 1.2).offset(start).limit(limit).all()

        count = db.session.query(Transaction.id) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 0.9, func.sum(FraudsDetail.scores) <= 1.2).count()
    elif types == 3:
        result = db.session.query(Transaction) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 1.5).offset(start).limit(
            limit).all()

        count = db.session.query(Transaction.id) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 1.5).count()
    else:
        result = db.session.query(Transaction) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 0.6).offset(start).limit(
            limit).all()

        count = db.session.query(Transaction.id) \
            .filter(Transaction.id == Fraud.transaction_id) \
            .filter(Fraud.fraud_id == FraudsDetail.id) \
            .group_by(Transaction.id) \
            .having(func.sum(FraudsDetail.scores) >= 0.6).count()

    data = []

    for i in result:
        data.append(format_transaction(i))

    obj = {}

    if count < start:
        obj['success'] = False
        obj['message'] = 'Error'
        return obj
    else:
        obj['success'] = True
        obj['currentPage'] = start
        obj['perPage'] = limit
        obj['totalData'] = count
        obj['totalPage'] = math.ceil(count / limit)
        # prev_link
        if start == 1:
            obj['previos'] = ''
        else:
            start_copy = max(1, start - limit)
            limit_copy = start - 1
            obj['previos'] = url + '?start=%d&limit=%d&types=%d' % (start_copy, limit_copy, types)

        # next_link
        if start + limit > count:
            obj['next'] = ''
        else:
            start_copy = start + limit
            obj['next'] = url + '?start=%d&limit=%d&types=%d' % (start_copy, limit, types)

        obj['transactions'] = data#[(start - 1): (start - 1 + limit)]
        return obj


def calculate_daily_statistics(start_date: date, end_date: date) -> [dict]:
    raw_query = text("""
    SELECT t.date::date as date,
    count(t.id) as total,
    count(case tf.fraud_id  when 1 then 1 else null end) as freq1,
    count(case tf.fraud_id  when 2 then 1 else null end) as freq2,
    count(case tf.fraud_id  when 3 then 1 else null end) as freq3,
    count(case tf.fraud_id  when 4 then 1 else null end) as freq4,
    count(case tf.fraud_id  when 5 then 1 else null end) as freq5,
    count(case tf.fraud_id  when 6 then 1 else null end) as freq6
    FROM transaction_frauds tf INNER JOIN transactions t on tf.transaction_id = t.id
    WHERE t."date"::date > :start_date and t."date"::date < :end_date
    GROUP BY t."date"::date
    """)
    raw_result = db.session.execute(raw_query, {'start_date': start_date, 'end_date': end_date}).mappings().all()
    return [dict(row) for row in raw_result]


def calculate_hourly_statistics(iso_date: date) -> [dict]:
    raw_query = text("""
    SELECT count(t.id) as freq, EXTRACT(hour FROM t."date")::int as date
    FROM transaction_frauds tf INNER JOIN transactions t on tf.transaction_id = t.id
    WHERE t."date"::date = :date_value
    GROUP BY EXTRACT(hour  FROM t."date")
    """)
    raw_result = db.session.execute(raw_query, {'date_value': iso_date}).mappings().all()
    return [dict(row) for row in raw_result]
