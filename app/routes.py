from app import app
from app.models import Transaction
from flask import request, jsonify
from datetime import datetime, date
from app.control import controller


@app.route('/search', methods=['GET'])
def get_transaction():
    clied_id = request.args.get('clied_id', str)
    try:
        return jsonify(
            controller.search_cliets(
                clied_id=clied_id
            )
        )
    except Exception as e:
        print(e)


@app.route('/statistics/daily', methods=['GET'])
def get_daily_statistics():
    start_date_param = request.args.get('startDate')
    end_date_param = request.args.get('endDate')
    if start_date_param is None or end_date_param is None:
        return jsonify({
            'success': False,
            'message': 'StartDate and endDate must be defined'
        })
    iso_start_date = datetime.fromisoformat(start_date_param).date()
    iso_end_date = datetime.fromisoformat(end_date_param).date()
    try:
        statistics = controller.calculate_daily_statistics(start_date=iso_start_date, end_date=iso_end_date)
        return jsonify(statistics)
    except Exception as e:
        print(e)


@app.route('/statistics/hourly', methods=['GET'])
def get_hourly_statistics():
    date_param = request.args.get('date')
    if date_param is None:
        return jsonify({
            'message': 'Date must be defined'
        })
    iso_date = datetime.fromisoformat(date_param).date()
    try:
        statistics = controller.calculate_hourly_statistics(iso_date=iso_date)
        return jsonify(statistics)
    except Exception as e:
        print(e)


@app.route('/transaction/page', methods=['GET'])
def get_transactions():
    start = request.args.get('start', 1, int)
    limit = request.args.get('limit', 10, int)
    order = request.args.get('order', 0, int)
    direction = request.args.get('direction', 0, int)

    try:
        return jsonify(
            controller.paginate_transactions(
                url='http://127.0.0.1:5000/transaction/page',
                order=order,
                direction=direction,
                start=start,
                limit=limit
            )
        )
    except Exception as e:
        print(e)


@app.route('/frauds/page', methods=['GET'])
def get_frauds():
    start = request.args.get('start', 1, int)
    limit = request.args.get('limit', 10, int)
    types = request.args.get('types', 0, int)
    try:
        res = controller.paginate_frauds(
            'http://127.0.0.1:5000/frauds/page',
            start=start,
            limit=limit,
            types=types
        )
        return jsonify(res)
    except Exception as e:
        print(e)
